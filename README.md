# Filtered RSS

[![Coverage](https://gitlab.com/kane-c/rss-filter/badges/main/coverage.svg)](https://gitlab.com/kane-c/rss-filter/commits/main)

Get an RSS feed with its items filtered.

Use positive and negative word filters to include or exclude items, so you only see what you care about.

Designed to be deployed as a Cloudflare Worker. Any push to `main` will trigger a deployment.

## Setup

1. Clone the repo
2. `npm ci`
3. Configure your feeds and filters per the below.
4. `npm start`
5. Point your reader to the feed by name (in the example below, it would be https://yoursite/test)

## Configuration

Create a `feeds.yml` file in the repo directory.

Populate it with a mapping of feed names to objects with a URL and array of filter objects.

Filters must have one or both of `contains` or `doesNotContain` keys, which are arrays of strings.

Example file:

```yaml
test:
  url: https://test.com/feed.xml
  filters:
    - contains:
        - word
        - abc
      doesNotContain:
        - xyz
    - contains:
        - yes
      doesNotContain:
        - no
```

## Code standards

- airbnb's ESLint rules
- 79 char line limit
- Prettier

## Tests

Tests run with Jest. Use `npm test`. Maintain high code coverage.
