import { parse } from 'yaml';

import './types.d';
import feedsFile from '../feeds.yml';

import getFeed from './feeds';

const feeds = parse(feedsFile, { merge: true }) as Record<string, Feed>;

export default {
  async fetch(request: Request) {
    const { pathname } = new URL(request.url);
    const name = pathname.substring(1);

    if (!name) {
      return new Response(JSON.stringify({ version: process.env.VERSION }), {
        headers: { 'Content-Type': 'application/json' },
      });
    }

    let xml;

    try {
      xml = await getFeed(feeds, name);
    } catch (e) {
      if (e instanceof Error) {
        if (e.message === 'Feed not found') {
          return new Response('', { status: 404 });
        }

        return new Response(e.stack, { status: 500 });
      }

      throw e;
    }

    return new Response(xml, {
      headers: { 'Content-Type': 'application/rss+xml' },
    });
  },
};
