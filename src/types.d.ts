interface BaseFeed {
  url: string;
}

interface FilteredFeed extends BaseFeed {
  filters: Filter[];
}

interface ReplaceFeed extends BaseFeed {
  raw: true;
  replace: string[];
}

interface CompiledReplaceFeed extends BaseFeed {
  replace: [RegExp, string][];
}

type Feed = FilteredFeed | ReplaceFeed;

declare module '*.yml' {
  const data: string;
  export default data;
}
