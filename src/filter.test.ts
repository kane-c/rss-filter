import {
  type CompiledFilter,
  buildReplaceRules,
  filterTitle,
  replace,
  shouldRejectTitle,
} from './filter';

const acceptRejectFilters = [
  {
    contains: ['XB', 'PS4'],
    doesNotContain: ['PC'],
  },
  {
    contains: ['Android', 'Google Play'],
    doesNotContain: ['iOS', 'Mac', 'iPhone', 'iPad', 'Apple', 'App Store'],
  },
  {
    contains: ['eBook', 'Audiobook', 'eBooks', 'Kindle'],
  },
  {
    contains: [
      'NSW',
      'NT',
      'SA',
      'WA',
      'TAS',
      'ACT',
      'QLD',
      'New South Wales',
      'Southern Australia',
      'Western Australia',
      'Tasmania',
      'Queensland',
      'Syd',
      'Sydney',
      'Adelaide',
      'Hobart',
      'Brisbane',
      'Canberra',
    ],
    doesNotContain: [
      'VIC',
      'Mel',
      'Melb',
      'Melbourne',
      'Victoria',
      '/flights?/',
      'fly',
    ],
  },
];

const acceptFilters = [
  {
    doesNotContain: ['Moom', 'Bar'],
  },
];

describe('filters', () => {
  it('contains filter', () => {
    expect.assertions(2);
    expect(shouldRejectTitle('test', /test/, null)).toBe(true);
    expect(shouldRejectTitle('foo', /test/, null)).toBeNull();
  });

  it('doesNotContain filter', () => {
    expect.assertions(2);
    expect(shouldRejectTitle('test', null, /test/)).toBe(false);
    expect(shouldRejectTitle('foo', null, /test/)).toBe(true);
  });

  it('both filters', () => {
    expect.assertions(3);
    expect(shouldRejectTitle('Android', /Android/, /iOS/)).toBe(true);
    expect(shouldRejectTitle('Android/iOS', /Android/, /iOS/)).toBe(false);
    expect(shouldRejectTitle('Something else', /Android/, /iOS/)).toBeNull();
  });

  it('accept filters', () => {
    expect.assertions(2);

    const cache: CompiledFilter[] = [];

    expect(filterTitle('Moom', acceptFilters, cache)).toBe(true);
    expect(filterTitle('Foo', acceptFilters, cache)).toBe(false);
  });

  it('accept/reject filters', () => {
    expect.assertions(5);

    const cache: CompiledFilter[] = [];

    expect(filterTitle('Android/iOS', acceptRejectFilters, cache)).toBe(true);
    expect(filterTitle('Android', acceptRejectFilters, cache)).toBe(false);
    expect(filterTitle('eBook', acceptRejectFilters, cache)).toBe(false);
    expect(filterTitle('[QLD]', acceptRejectFilters, cache)).toBe(false);
    expect(filterTitle('Foo', acceptRejectFilters, cache)).toBe(true);
  });

  it('regexp filters', () => {
    expect.assertions(3);

    const cache: CompiledFilter[] = [];

    expect(filterTitle('NSW Flights', acceptRejectFilters, cache)).toBe(true);
    expect(filterTitle('NSW Flight', acceptRejectFilters, cache)).toBe(true);
    expect(filterTitle('NSW Notflights', acceptRejectFilters, cache)).toBe(
      false,
    );
  });

  it('replace filters', () => {
    expect.assertions(1);

    const rules = ['abcd', 'efgh', 'qwer', 'tyui'];
    const cache: [RegExp, string][] = buildReplaceRules(rules);

    expect(replace(cache, '<tag>blahabcdblah</tag><qwer>Test</qwer>')).toBe(
      '<tag>blahefghblah</tag><tyui>Test</tyui>',
    );
  });
});
