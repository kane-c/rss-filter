interface BaseFeed {
  url: string;
}

export interface FilteredFeed extends BaseFeed {
  filters: Filter[];
}

interface ReplaceFeed extends BaseFeed {
  raw: true;
  replace: string[];
}

export interface CompiledReplaceFeed extends BaseFeed {
  replace: [RegExp, string][];
}

export type Feed = FilteredFeed | ReplaceFeed;

export function isReplaceFeed(feed: Feed): feed is ReplaceFeed {
  return 'replace' in feed;
}

export function escapeRegExp(string: string): string {
  // Escape a string for use in a RegExp, unless it is `/like this/` then
  // assume it is already a RegExp
  if (string.startsWith('/') && string.endsWith('/')) {
    return string.substring(1, string.length - 1);
  }

  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& = whole match
}

function stringsToRegExp(strings: string[]): RegExp {
  // Build a RegExp out of an array of strings
  // /\b(word1|word2|...)\b/i
  return new RegExp(`\\b(${strings.map(escapeRegExp).join('|')})\\b`, 'i');
}

function compileFilters(contains?: string[], doesNotContain?: string[]) {
  const compiledContains =
    contains && contains.length ? stringsToRegExp(contains) : null;
  const compiledDoesNotContain =
    doesNotContain && doesNotContain.length
      ? stringsToRegExp(doesNotContain)
      : null;

  return {
    contains: compiledContains,
    doesNotContain: compiledDoesNotContain,
  };
}

type FilterA = {
  contains: string[];
};

type FilterB = {
  doesNotContain: string[];
};

export type Filter = FilterA | FilterB | (FilterA & FilterB);

export interface CompiledFilter {
  contains: null | RegExp;
  doesNotContain: null | RegExp;
}

export function shouldRejectTitle(
  title: string,
  contains: null | RegExp,
  doesNotContain: null | RegExp,
) {
  if (!contains && doesNotContain) {
    return !doesNotContain.test(title);
  }

  if (!doesNotContain) {
    if (contains?.test(title)) {
      return true;
    }

    return null;
  }

  if (contains?.test(title)) {
    return !doesNotContain.test(title);
  }

  return null;
}

export function filterTitle(
  title: string,
  filters: Filter[],
  cache: CompiledFilter[],
) {
  for (let i = 0; i < filters.length; i += 1) {
    const filter = filters[i];

    if (!cache[i]) {
      const { contains, doesNotContain } = compileFilters(
        'contains' in filter ? filter.contains : undefined,
        'doesNotContain' in filter ? filter.doesNotContain : undefined,
      );

      // eslint-disable-next-line no-param-reassign
      cache[i] = {
        contains,
        doesNotContain,
      };
    }

    const { contains, doesNotContain } = cache[i];

    const rejected = shouldRejectTitle(title, contains, doesNotContain);

    if (rejected !== null) {
      return !rejected;
    }
  }

  return true;
}

export function buildReplaceRules(pairs: string[]) {
  if (pairs.length % 2 !== 0) {
    throw new Error('Uneven set of replacement rules');
  }

  // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
  return pairs
    .reduce((result, _, index, array) => {
      if (index % 2 === 0) {
        result.push(array.slice(index, index + 2));
      }

      return result;
    }, [] as string[][])
    .map(([find, replacement]) => [
      new RegExp(escapeRegExp(find), 'g'),
      replacement,
    ]) as [RegExp, string][];
}

export function replace(
  replacements: CompiledReplaceFeed['replace'],
  xml: string,
) {
  let newXml = xml;

  replacements.forEach(([find, replacement]) => {
    newXml = newXml.replace(find, replacement);
  });

  return newXml;
}
