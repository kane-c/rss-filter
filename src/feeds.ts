import { XMLParser } from 'fast-xml-parser';

import {
  type CompiledFilter,
  type Feed,
  buildReplaceRules,
  filterTitle,
  isReplaceFeed,
  replace,
} from './filter';

interface RssItem {
  content?: string;
  guid?: string;
  link?: string;
  pubDate?: string;
  title?: string;
}

interface RssFeed {
  description?: string;
  item?: RssItem[];
  link?: string;
  title?: string;
}

interface FullRssFeed {
  rss: {
    channel: RssFeed;
  };
}

const parser = new XMLParser();
const filterRegExpCache: Record<
  string,
  (CompiledFilter | [RegExp, string])[]
> = {};

function escapeHtml(html: string) {
  return html
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#039;');
}

function httpToHttps(url: string) {
  return url.replace('http:', 'https:');
}

function processItem(item: RssItem) {
  return `<item>
    <title>${escapeHtml(item.title || '')}</title>
    <link>${escapeHtml(httpToHttps(item.link || ''))}</link>
    ${
      item.content
        ? `<description><![CDATA[${item.content || ''}]]></description>`
        : ''
    }
    <pubDate>${escapeHtml(item.pubDate || '')}</pubDate>
    <guid>${escapeHtml(item.guid || '')}</guid>
  </item>`;
}

function processFeed(rssFeed: RssFeed, items: RssItem[]) {
  return `<?xml version="1.0" encoding="UTF-8" ?>
  <rss version="2.0">
    <channel>
      <title>${escapeHtml(rssFeed.title || '')}</title>
      <link>${escapeHtml(httpToHttps(rssFeed.link || ''))}</link>
      <description>${escapeHtml(rssFeed.description || '')}</description>
      ${items.map(processItem).join('\n')}
    </channel>
  </rss>`;
}

export default async function getFeed(
  feeds: Record<string, Feed>,
  name: string,
) {
  const feed = feeds[name];

  if (!feed) {
    throw new Error('Feed not found');
  }

  // Need to manually fetch the feed to run on Cloudflare workers
  const response = await fetch(feed.url, {
    headers: {
      // Required for some sites
      'User-Agent':
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) ' +
        'AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.0 ' +
        'Safari/605.1.15',
    },
  });
  let xml = await response.text();
  const rssFeed = ((await parser.parse(xml)) as FullRssFeed).rss.channel;

  if (!filterRegExpCache[name]) {
    filterRegExpCache[name] = [];
  }

  const cache = filterRegExpCache[name];

  if (isReplaceFeed(feed)) {
    if (!cache.length) {
      cache.push(...buildReplaceRules(feed.replace));
    }

    xml = replace(cache as [RegExp, string][], xml);
  } else {
    const { item } = rssFeed;
    const filteredItems = feed.filters
      ? item?.filter(
          ({ title }) =>
            !title ||
            filterTitle(title, feed.filters, cache as CompiledFilter[]),
        )
      : item;

    xml = processFeed(rssFeed, filteredItems ?? []);
  }

  // Minify
  xml = xml.replace(/>\s+</g, '><');

  return xml;
}
